import { BrowserRouter, Route, Routes } from "react-router-dom";
import Footer from "./components/footer";
import Navbar from "./components/navbar";
// import Header from "./components/header";
import Home from "./pages/home";
import Cars from "./pages/cars";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import store from "./app/store";

function App() {
  const currentTitle = useSelector(state => state.ui.title)
  useEffect(() => {
    store.subscribe(() => {
      const state = store.getState()
      document.title = state.ui.title
    })
    document.title = currentTitle
  }, [currentTitle])
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/cars" element={<Cars />} />
      </Routes>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
