import { useEffect } from "react"

const Home = () => {
    useEffect(() => {
        if (window.loadOwlCarousel) {
            window.loadOwlCarousel()
        }
    }, [])
    return (
        <><div className="container" data-aos="fade-up" data-aos-duration="1000">
            <div className="row posOS">
                <div className="col" id="girl-pict">
                    <img alt="" className="perempuan" src="/style/image/img_service.png" />
                </div>
                <div className="col" id="best-rental">
                    <h3 style={{ fontWeight: 'bold' }}>Best Car Rental for any kind of trip in Palembang!</h3><br />
                    <p className="sewa">
                        Sewa mobil di Palembang bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain,
                        kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding,
                        meeting, dll.
                    </p>
                    <p> <img src="/style/image/checklist.svg" alt="" /> &nbsp; &nbsp; &nbsp; Sewa Mobil Dengan Supir di Bali
                        12
                        Jam </p>
                    <p> <img src="/style/image/checklist.svg" alt="" /> &nbsp; &nbsp; &nbsp; Sewa Mobil Lepas Kunci di Bali 24
                        Jam
                    </p>
                    <p> <img src="/style/image/checklist.svg" alt="" /> &nbsp; &nbsp; &nbsp; Sewa Mobil Jangka Panjang Bulanan
                    </p>
                    <p> <img src="/style/image/checklist.svg" alt="" /> &nbsp; &nbsp; &nbsp; Gratis Antar - Jemput Mobil di
                        Bandara</p>
                    <p> <img src="/style/image/checklist.svg" alt="" /> &nbsp; &nbsp; &nbsp; Layanan Airport Transfer / Drop
                        In
                        Out</p>
                </div>
            </div>
        </div><div>
                <div className="judul" data-aos="fade-up" data-aos-duration="1500">
                    <h3 className="judulWU" style={{ fontWeight: 'bold' }}>Why Us?</h3>
                    <p>
                        Mengapa harus pilih Binar Car Rental?
                    </p>
                </div>
                <div className="row-cols-4 whyuscontainer">
                    <div className="col-md-12 card" data-aos="fade-right" data-aos-duration="1500">
                        <div className="card-body kartu">
                            <h5 className="card-title WU" style={{ paddingBottom: '10' }}> <img alt="" className="logoWU"
                                src="/style/image/Mobil-Lengkap.svg" /></h5>
                            <h6 className="card-subtitle mb-2 caption">Mobil Lengkap</h6>
                            <p className="card-text">Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                        </div>
                    </div>
                    <div className="col-md-12 card" data-aos="fade-right" data-aos-duration="1500">
                        <div className="card-body kartu">
                            <h5 className="card-title WU" style={{ paddingBottom: '10' }}> <img alt="" className="logoWU"
                                src="/style/image/Harga-Murah.svg" /></h5>
                            <h6 className="card-subtitle mb-2 caption">Harga Murah</h6>
                            <p className="card-text">Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain
                            </p>
                        </div>
                    </div>
                    <div className="col-md-12 card" data-aos="fade-left" data-aos-duration="1500">
                        <div className="card-body kartu">
                            <h5 className="card-title WU" style={{ paddingBottom: '10' }}> <img alt="" className="logoWU"
                                src="/style/image/Layanan.svg" /></h5>
                            <h6 className="card-subtitle mb-2 caption">Layanan 24 Jam</h6>
                            <p className="card-text">Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir
                                minggu</p>
                        </div>
                    </div>
                    <div className="col-md-12 card" data-aos="fade-left" data-aos-duration="1500">
                        <div className="card-body kartu">
                            <h5 className="card-title WU" style={{ paddingBottom: '10' }}> <img alt="" className="logoWU"
                                src="/style/image/Sopir-Profesional.svg" /></h5>
                            <h6 className="card-subtitle mb-2 caption">Sopir Profesional</h6>
                            <p className="card-text">Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                        </div>
                    </div>
                </div>
            </div><br /><br />
            <div id="owl-container">
                <div className="owl-carousel owl-theme">
                    <div className="item">
                        <div className="">
                            <div className="container">
                                <div className="row testi">
                                    <div className="col-sm-3 mt-4" id="foto">
                                        <img className="foto" src="/style/image/testi1.svg" alt="" /></div>
                                    <div className="col-sm-9" id="konten">
                                        <div className="row" style={{ textAlign: 'left' }}>
                                            <div className="bintang">
                                                <img className="star" src="/style/image/bintang.svg" alt="" /></div>
                                            <div className="col-12 col-sm-12 testiContent"
                                                style={{ textAlign: 'left', paddingRight: '15' }}>
                                                <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius
                                                    quo, cumque quam illo atque ab sunt ratione. Sed sapiente,
                                                    repudiandae modi sint suscipit nesciunt fuga deserunt fugiat
                                                    molestias, facere maxime."</p>
                                                <p style={{ fontWeight: 'bold' }}>John Dee 32, Bromo</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="">
                            <div className="container">
                                <div className="row testi">
                                    <div className="col-sm-3 mt-4" id="foto">
                                        <img className="foto" src="/style/image/testi2.svg" alt=""
                                        /></div>
                                    <div className="col-sm-9" id="konten">
                                        <div className="row" style={{ textAlign: 'left' }}>
                                            <div className="bintang">
                                                <img className="star" src="/style/image/bintang.svg" alt=""
                                                /></div>
                                            <div className="col-12  testiContent" style={{ textAlign: 'left', paddingRight: 15 }}>
                                                <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius
                                                    quo, cumque quam illo atque ab sunt ratione. Sed sapiente,
                                                    repudiandae modi sint suscipit nesciunt fuga deserunt fugiat
                                                    molestias, facere maxime."</p>
                                                <p style={{ fontWeight: 'bold' }}>John Dee 33, Bromo</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="">
                            <div className="container">
                                <div className="row testi">
                                    <div className="col-sm-3 mt-4" id="foto">
                                        <img className="foto" src="/style/image/testi1.svg" alt=""
                                        /></div>
                                    <div className="col-sm-9" id="konten">
                                        <div className="row" style={{ textAlign: 'left' }}>
                                            <div className="bintang">
                                                <img className="star" src="/style/image/bintang.svg" alt=""
                                                /></div>
                                            <div className="col-12 col-sm-12 testiContent"
                                                style={{ textAlign: 'left', paddingRight: 15 }}>
                                                <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius
                                                    quo, cumque quam illo atque ab sunt ratione. Sed sapiente,
                                                    repudiandae modi sint suscipit nesciunt fuga deserunt fugiat
                                                    molestias, facere maxime."</p>
                                                <p style={{ fontWeight: 'bold' }}>John Dee 32, Bromo</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="item">
                        <div className="">
                            <div className="container">
                                <div className="row testi">
                                    <div className="col-sm-3 mt-4" id="foto">
                                        <img className="foto" src="/style/image/testi2.svg" alt=""
                                        /></div>
                                    <div className="col-sm-9" id="konten">
                                        <div className="row" style={{ textAlign: 'left' }}>
                                            <div className="bintang">
                                                <img className="star" src="/style/image/bintang.svg" alt=""
                                                /></div>
                                            <div className="col-12  testiContent" style={{ textAlign: 'left', paddingRight: 15 }}>
                                                <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Eius
                                                    quo, cumque quam illo atque ab sunt ratione. Sed sapiente,
                                                    repudiandae modi sint suscipit nesciunt fuga deserunt fugiat
                                                    molestias, facere maxime."</p>
                                                <p style={{ fontWeight: 'bold' }}>John Dee 33, Bromo</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><div className="banner" data-aos="fade-up" data-aos-duration="1000">
                <h3 className="judul-banner">Sewa Mobil di Palembang Sekarang</h3>
                <p className="isi-banner">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aperiam delectus facilis enim
                    dolorem iste consequatur? Dolorem, nihil quasi? Nesciunt, enim.</p>
                <button className="btn btn-success me-2 tombolCTA" type="button">Mulai Sewa Mobil</button>
            </div><br /><br /><div className="container" data-aos="fade-up" data-aos-duration="1000" style={{ marginTop: 100 }}>
                <div className="row flexFAQ">
                    <div className="col" id="faq">
                        <h3><b>Frequently Asked Question</b></h3>
                        <p style={{ paddingTop: 10 }}>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</p>
                    </div>
                    <div className="col listFAQ">
                        <div className="accordion" id="accordionExample">
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingOne">
                                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Apa saja syarat yang dibutuhkan?
                                    </button>
                                </h2>
                                <div id="collapseOne" className="accordion-collapse collapse" aria-labelledby="headingOne">
                                    <div className="accordion-body">
                                        <strong>This is the first item's accordion body.</strong> It is shown by default, until
                                        the collapse plugin adds the appropriate classNamees that we use to style each element.
                                        These classNamees control the overall appearance, as well as the showing and hiding via CSS
                                        transitions. You can modify any of this with custom CSS or overriding our default
                                        variables. It's also worth noting that just about any HTML can go within the
                                        <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingTwo">
                                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"
                                        style={{ borderTop: '1px solid #dcdcdc' }}>
                                        Berapa hari minimal sewa mobil lepas kunci?</button>
                                </h2>
                                <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo">
                                    <div className="accordion-body">
                                        <strong>This is the second item's accordion body.</strong> It is hidden by default,
                                        until the collapse plugin adds the appropriate classNamees that we use to style each
                                        element. These classNamees control the overall appearance, as well as the showing and hiding
                                        via CSS transitions. You can modify any of this with custom CSS or overriding our
                                        default variables. It's also worth noting that just about any HTML can go within the
                                        <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingThree">
                                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"
                                        style={{ borderTop: '1px solid #dcdcdc' }}>
                                        Berapa hari sebelumnya sabaiknya booking sewa mobil?
                                    </button>
                                </h2>
                                <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree">
                                    <div className="accordion-body">
                                        <strong>This is the third item's accordion body.</strong> It is hidden by default, until
                                        the collapse plugin adds the appropriate classNamees that we use to style each element.
                                        These classNamees control the overall appearance, as well as the showing and hiding via CSS
                                        transitions. You can modify any of this with custom CSS or overriding our default
                                        variables. It's also worth noting that just about any HTML can go within the
                                        <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingFour">
                                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour"
                                        style={{ borderTop: '1px solid #dcdcdc' }}>
                                        Apakah Ada biaya antar-jemput?
                                    </button>
                                </h2>
                                <div id="collapseFour" className="accordion-collapse collapse" aria-labelledby="headingFour">
                                    <div className="accordion-body">
                                        <strong>This is the third item's accordion body.</strong> It is hidden by default, until
                                        the collapse plugin adds the appropriate classNamees that we use to style each element.
                                        These classNamees control the overall appearance, as well as the showing and hiding via CSS
                                        transitions. You can modify any of this with custom CSS or overriding our default
                                        variables. It's also worth noting that just about any HTML can go within the
                                        <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                            <div className="accordion-item">
                                <h2 className="accordion-header" id="headingFive">
                                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                        data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive"
                                        style={{ borderTop: '1px solid #dcdcdc' }}>
                                        Bagaimana jika terjadi kecelakaan
                                    </button>
                                </h2>
                                <div id="collapseFive" className="accordion-collapse collapse" aria-labelledby="headingFive">
                                    <div className="accordion-body">
                                        <strong>This is the third item's accordion body.</strong> It is hidden by default, until
                                        the collapse plugin adds the appropriate classNamees that we use to style each element.
                                        These classNamees control the overall appearance, as well as the showing and hiding via CSS
                                        transitions. You can modify any of this with custom CSS or overriding our default
                                        variables. It's also worth noting that just about any HTML can go within the
                                        <code>.accordion-body</code>, though the transition does limit overflow.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></>
    )
}

export default Home
