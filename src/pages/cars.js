import '../components/navbar';
import '../components/footer';
import CarList from '../components/car';
import { useDispatch } from 'react-redux';
import { useEffect, useState } from 'react';
import { setFilter } from "../reducer/api-store";

const Cars = () => {
    const dispatch = useDispatch();
    const [driver, setDriver] = useState("")
    const [date, setDate] = useState("")
    const [time, setTime] = useState("")
    const [passenger, setPassenger] = useState("")
    const [data, setData] = useState({});

    useEffect(() => {
        dispatch(setFilter(data));
    }, [dispatch, data]);

    function showBackdrop() {
        document.querySelector(".modal-backdrop").style.display = "block"
        document.body.classList.add("modal-open")
    }
    function hideBackdrop(e) {
        document.querySelector(".modal-backdrop").style.display = "none"
        document.body.classList.remove("modal-open")
        e.target.blur()
    }
    const onSearch = async (e) => {
        console.log("halo")
        e.preventDefault();
        setData({
            driver: driver,
            date: date,
            time: time,
            passenger: parseInt(passenger),
        });
    }
    return (
        <><div className="container mt-4" style={{ marginLeft: 130 }}>
            <div id="filter-box">
                <div className="card mb-4">
                    <div className="card-body filter">
                        <div className="row">
                            <div className="col-lg-10 col-md-12">
                                <div className="row">
                                    <div className="col-lg-3 col-md-6">
                                        <div className="form-group" id="form-group1"><label>Tipe Driver</label><select
                                            id="select-driver" className="form-control" onFocus={showBackdrop} onBlur={hideBackdrop} onChange={(e) => setDriver(e.target.value)}>
                                            <option>Pilih Tipe Driver</option>
                                            <option value="Dengan Sopir">Dengan Sopir</option>
                                            <option value="Tanpa Sopir">Tanpa Sopir</option>
                                        </select></div>
                                    </div>
                                    <div className="col-lg-3 col-md-6">
                                        <div className="form-group" id="form-group2"><label>Tanggal</label><input id="input-tanggal"
                                            type="date" className="form-control" onFocus={showBackdrop} onBlur={hideBackdrop} onChange={(e) => setDate(e.target.value)} /></div>
                                    </div>
                                    <div className="col-lg-3 col-md-6">
                                        <div className="form-group" id="form-group3"><label>Waktu Jemput/Ambil</label><input
                                            id="input-jemput" type="time" className="form-control" onFocus={showBackdrop} onBlur={hideBackdrop} onChange={(e) => setTime(e.target.value)} /></div>
                                    </div>
                                    <div className="col-lg-3 col-md-6">
                                        <div className="form-group" id="form-group4"><label>Jumlah Penumpang</label>
                                            <div className="input-group"><input id="input-penumpang" type="number"
                                                className="form-control" placeholder="Jumlah Penumpang" onFocus={showBackdrop} onBlur={hideBackdrop} onChange={(e) => setPassenger(e.target.value)} />
                                                <div className="input-group-append">
                                                    <div className="input-group-text bg-white"><img src="style/image/user-icon.svg" alt='' />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-2 col-md-12">
                                <div className="form-group" id="form-group5"><label>&nbsp;</label><button id="button-cari"
                                    type="button" className="btn btn-success col-sm-12" onClick={onSearch}>Cari Mobil</button></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <CarList />
        </div>
            <div className="modal-backdrop background-filter"></div>
            <center><div id="cars-container"></div></center>
        </>
    )
}

export default Cars