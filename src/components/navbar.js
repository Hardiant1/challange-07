// import { useSelector } from "react-redux"
import { Link } from "react-router-dom"
const Navbar = () => {
    return (
        <div className="hero-section">
            <br /><br /><br />
            <nav className="navbar fixed-top navbar-expand-lg bg-transparent p-md-4">
                <div className="container-fluid">
                    <Link className="navbar-brand logoBinar" to="/">
                        <img src="/style/image/Binar.png" width="90" height="50" alt="" />
                    </Link>
                    <button className="navbar-toggler-right navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false"
                        aria-label="Toggle navigation" style={{ marginRight: '10px' }}>
                        <img src="/style/image/burger.svg" className="navbar-toggler-icon" alt=""></img>
                    </button>
                    <div className="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
                        <div className="navbar-nav" id="menu" style={{ marginRight: '100px' }}>
                            <li className="d-lg-none">
                                <br />
                                <Link className="bcr" to="/#" style={{ marginLeft: '10px' }}><strong>BCR</strong></Link>
                                <Link className="navbar-nav-toggler cross" to="/#"><img src="/style/image/cross.svg" alt="" /></Link>
                            </li>
                            <Link className="nav-link" to="/#">Our Services</Link>
                            <Link className="nav-link" to="/#">Why Us</Link>
                            <Link className="nav-link" to="/#">Testimonial</Link>
                            <Link className="nav-link" to="/#">FAQ</Link>
                            <button className="btn btn-success me-2" type="button">Register</button>
                        </div>
                    </div>
                </div>
            </nav>
            <div className="container-fluid">
                <div className="row" id="hero">
                    <div className="col col-lg" id="welcome">
                        <h1>Sewa & Rental Mobil terbaik di
                            Kawasan Palembang</h1>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
                            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk
                            sewa mobil selama 24 jam.</p>
                        <Link to="/cars"><button className="btn btn-success me-2" type="button">Mulai Sewa
                            Mobil</button></Link>
                    </div>
                    <div className="col col-lg" id="car">
                        <img className="mobil" src="/style/image/car.svg" alt="" />
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Navbar
