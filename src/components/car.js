import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAsyncData } from "../reducer/api-store";

function rupiah(price) {
    let convertPrice = price.toString();
    let convertString = convertPrice.split("");
    let array = [];
    let temp = 3;

    for (let i = convertString.length - 1; i >= 0; i--) {
        temp -= 1;
        array.unshift(convertString[i]);
        if (temp === 0 && i !== 0) {
            array.unshift(".");
            temp = 3;
        }
    }
    return array.join("");
}
const CarList = () => {
    const dispatch = useDispatch();
    const listCarsJson = useSelector((state) => state.api.filterCars);

    useEffect(() => {
        dispatch(getAsyncData());
    }, [dispatch]);
    return (
        <>
            <div className="row" id="cars-list">
                {listCarsJson.map((car) => {
                    return (
                        <div className="col-sm-4 col-sm-4 col-sm-4 mt-3" key={car.id}>
                            <div className="card" style={{ width: 360, marginTop: 40 }} >
                                <div style={{ marginLeft: 20, marginRight: 20, marginTop: 20, marginBottom: 0 }}><center>
                                    <img src={car.image} alt={car.manufacture} style={{ height: 160, width: 300 }} /></center>
                                    <div className="card-body" style={{ paddingTop: 50, paddingRight: 0, paddingLeft: 0 }}>
                                        <p>{car.model} / {car.plate}</p>
                                        <p><strong>Rp {rupiah(car.rentPerDay)} / hari </strong></p>
                                        <p style={{ height: 75 }}>{car.description}</p>
                                        <div className="row">
                                            <div className="col-sm-8"><p><img src="/style/image/user-icon.svg" alt="/" />&nbsp;{car.capacity} orang</p></div>
                                            <div className="col-sm-8"><p><img src="/style/image/setting-icon.svg" alt="/" />&nbsp;{car.transmission}</p></div>
                                            <div className="col-sm-8"><p><img src="/style/image/calendar-icon.svg" alt="/" />&nbsp;Tahun&nbsp;{car.year}</p></div>
                                        </div>
                                        <div className="btn-success btn-lg"><center>Pilih Mobil</center></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>

        </>
    );
};

export default CarList;
