const Footer = () => {
    return (
        <><div className="footer">
            <div className="container">
                <div className="row listFooter">
                    <div className="col-4">
                        <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                        <p>binarcarrental@gmail.com</p>
                        <p>081-233-334-808</p>
                    </div>
                    <div className="col-2">
                        <p><b>Our services</b></p>
                        <p><b>Why Us</b></p>
                        <p><b>Testimonial</b></p>
                        <p><b>FAQ</b></p>
                    </div>
                    <div className="col-4">
                        <p>Connect with us</p>
                        <img src="/style/image/fb-icon.svg" alt="" />&nbsp;
                        <img src="/style/image/ig-icon.svg" alt="" />&nbsp;
                        <img src="/style/image/twitter-icon.svg" alt="" />&nbsp;
                        <img src="/style/image/mail-icon.svg" alt="" />&nbsp;
                        <img src="/style/image/twitch-icon.svg" alt="" />
                    </div>
                    <div className="col-2 copyright">
                        <p>Copyright Binar 2022</p>
                        <img src="/style/image/Binar.png" style={{ width: 100, height: 60 }} alt="" />
                    </div>
                </div>
            </div>
        </div><br /><br />
        </>
    )
}

export default Footer
