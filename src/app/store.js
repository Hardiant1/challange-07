import { configureStore } from "@reduxjs/toolkit";
import uiStore from "../reducer/ui-store"
import apiStore from "../reducer/api-store"

const store = configureStore({
    reducer: {
        ui: uiStore,
        api: apiStore,
    }
})
export default store
